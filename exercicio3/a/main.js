var calcular = document.getElementById('calcula');
calcular.addEventListener("click", function(){
                                        calcula();
                                    });
var table;


function calcula(){

    var n1 = +document.getElementById('numero1').value;   // o "+" no comeco forca o tipo numerico
    var n2 = +document.getElementById('numero2').value;

    if(table == null){
        table = document.createElement('table');
        var headerRow = document.createElement('tr');
        var op = headerRow.appendChild(document.createElement('th'));
        op.appendChild(document.createTextNode("Operação"));
        var valor = headerRow.appendChild(document.createElement('th'));
        valor.appendChild(document.createTextNode("Valor"));

        var tSoma = document.createElement('tr');
        var soma = document.createElement('td');
        soma.id = 'somaTexto';
        soma.appendChild(document.createTextNode(n1+" + "+ n2));
        var resultSoma = document.createElement('td');
        resultSoma.id = 'soma';
        resultSoma.appendChild(document.createTextNode(n1 + n2));
        tSoma.appendChild(soma);
        tSoma.appendChild(resultSoma);

        var tMult = document.createElement('tr');
        var mult = document.createElement('td');
        mult.id = 'multTexto';
        mult.appendChild(document.createTextNode(n1+" * "+ n2));
        var resultMult = document.createElement('td');
        resultMult.appendChild(document.createTextNode(n1 * n2));
        resultMult.id = 'mult';
        tMult.appendChild(mult);
        tMult.appendChild(resultMult);

        var tDiv = document.createElement('tr');
        var div = document.createElement('td');
        div.id = 'divTexto';
        div.appendChild(document.createTextNode(n1+" / "+ n2));
        var resultDiv = document.createElement('td');
        resultDiv.id = 'div';
        resultDiv.appendChild(document.createTextNode((n1 / n2).toFixed(1)));
        tDiv.appendChild(div);
        tDiv.appendChild(resultDiv);

        var tResto = document.createElement('tr');
        var resto = document.createElement('td');
        resto.id = 'restoTexto';
        resto.appendChild(document.createTextNode(n1+" % "+ n2));
        var resultResto = document.createElement('td');
        resultResto.appendChild(document.createTextNode((n1 % n2).toFixed(1)));
        resultResto.id = 'resto';
        tResto.appendChild(resto);
        tResto.appendChild(resultResto);


        
        table.appendChild(headerRow);
        table.appendChild(tSoma);
        table.appendChild(tMult);
        table.appendChild(tDiv);
        table.appendChild(tResto);

        document.body.appendChild(table);
    }
    else{
        document.getElementById('soma').innerText = (n1 + n2);
        document.getElementById('mult').innerText = (n1 * n2);
        document.getElementById('div').innerText = (n1 / n2).toFixed(1);
        document.getElementById('resto').innerText = (n1 % n2).toFixed(1);

        document.getElementById('somaTexto').innerHTML = (n1+" + "+ n2);
        document.getElementById('multTexto').innerHTML = (n1+" * "+ n2);
        document.getElementById('divTexto').innerHTML = (n1+" / "+ n2);
        document.getElementById('restoTexto').innerHTML = (n1+" % "+ n2);

        
    }
    
}



